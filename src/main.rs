mod constants;

use crate::constants::components::COMPONENTS;
use crate::constants::unicode2cns::UNICODE2CNS;
use anyhow::Result;
use std::io;

struct Hanzi {
    unicode: String,
    cns: String,
    // radical: u32,
    components: Vec<u32>,
}

fn get_unicode(hanzi: &str) -> Result<String> {
    let c = hanzi.chars().next().expect("No hanzi");
    let hex = format!("{:x}", c as u32).to_uppercase();
    Ok(hex)
}

fn get_cns(unicode: &str) -> &'static str {
    UNICODE2CNS
        .get(unicode)
        .expect("Can't get Unicode code point")
}

fn get_comps(cns: &str) -> &'static str {
    COMPONENTS.get(cns).expect("Can't get components")
}

fn get_input() -> Result<String> {
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    Ok(input)
}

fn main() -> Result<()> {
    let hanzi = get_input()?;
    let unicode = get_unicode(&hanzi)?;
    let cns = get_cns(&unicode);
    let components = get_comps(cns);

    println!("cns: {}", cns);
    println!("components: {}", components);

    Ok(())
}
